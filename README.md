# Author - Ayman Aljohary
- Live: https://parenteu.herokuapp.com/
- Progressive Web App (100%)
- Server Side Rendering (100%)
- Redux (100%)
- Unit Testing (30%)
- CI/CD (100%)
- Ability to deploy in Docker container (100%)

![APP Performance](http://mighty-citadel-50580.herokuapp.com/assets/imgs/img.png)
