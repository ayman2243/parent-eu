export class User {
    id: string;
    first_name: string;
    last_name: string;
    avatar: string;

    constructor(input: any) {
        Object.assign(this, input);
        return this;
    }
}
