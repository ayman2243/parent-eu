import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Garuds
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

// Auth Routes
import { LoginComponent } from './modules/auth/login/login.component';
import { LogoutComponent } from './modules/auth/logout/logout.component';

// Dashboard Layout Component
import { LayoutComponent } from './modules/dashboard/layout/layout.component';

// User Module Routes
import { AddComponent as AddUserComponent } from './modules/dashboard/users/add/add.component';
import { ListComponent as ListUserComponent } from './modules/dashboard/users/list/list.component';
import { EditComponent as EditUserComponent } from './modules/dashboard/users/edit/edit.component';
import { ViewComponent as ViewUserComponent } from './modules/dashboard/users/view/view.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
  {
    path: 'dashboard',
    component: LayoutComponent,
    canActivate: [AuthGuard], 
    children: [
      {path: '', redirectTo: 'users', pathMatch: 'full'},
      {
        path: 'users',
        children:[
          {path: '', redirectTo: 'list', pathMatch: 'full'},
          {path: 'list', children:[
            {path: '', redirectTo: '1', pathMatch: 'full'},
            {path: ':page', component: ListUserComponent}
          ]},
          {path:'add', component: AddUserComponent},
          {path:'view/:id', component: ViewUserComponent},
          {path:'edit/:id', component: EditUserComponent}
        ]
      }
    ]
  },
  { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
