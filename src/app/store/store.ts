import { ADD_USER, UPDATE_USER, DELETE_USER, REMOVE_ALL_USERS } from './actions/actions';
import { User } from '../models/user.model';

export interface IAppState {
    users: User[];
}

export const INITIAL_STATE: IAppState = {
    users: []
}

export function rootReducer(state: IAppState, action): IAppState {

    switch (action.type) {

        case ADD_USER:
            action.user.id = state.users.length + 1 + '_';
            state.users.push(action.user);
            return state
            
        case UPDATE_USER:
            var user = state.users.find(t => t.id === action.id);
            var index = state.users.indexOf(user);
            state[index] = action.user;
            return state;

        case DELETE_USER:
            return Object.assign({}, state, {
                users: state.users.filter(it => it.id !== action.id)
            });
    }

    return state;
}