import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { AppRoutingModule } from './../../app-routing.module';
import { UsersModule } from './users/users.module';

import { CollapseModule, BsDropdownModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    CollapseModule,
    UsersModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [LayoutComponent]
})
export class DashboardModule { }
