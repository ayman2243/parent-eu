import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { UsersService } from '../../../../services/users.service';
import { User } from '../../../../models/user.model';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../../../store/store';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @select('users') usersInStore;
  currentPageNumber = null;
  paginate2;
  users: Array<User>;
  pagination: any;
  constructor(
    private _users: UsersService,
    private router: Router,
    private ngRedux: NgRedux<IAppState>,
    private route: ActivatedRoute) {
      this.router.onSameUrlNavigation = "reload";
      this.route.url.subscribe((data) => {
        this.currentPageNumber = Number(this.route.snapshot.params['page']);
        this.getUsers(this.currentPageNumber);
      });
  }

  getUsers(page = 1){
    this._users.list(page).subscribe(
      users => {
        this.usersInStore.subscribe((usersInStore) =>{
          if(users['docs'].length === 0){
            page = page - 4;
            this.users = usersInStore.slice(((page - 1) * 3), ((page - 1) * 3) + 3);
          }
          else
            this.users = users['docs'];
          this.pagination = users['pagination'];
          this.pagination['total'] = (Number(this.pagination['total']) + usersInStore.length);
        });
        if( this.currentPageNumber > Math.ceil(this.pagination['total']/this.pagination['per_page']) )
          this.router.navigate([`./dashboard/users/list/1`]);
      },
      err => console.log(err),
    );
  }

  pageChanged(e) {
    this.router.navigate([`./dashboard/users/list/${e.page}`]);
  }

  viewUser(id){
    this.router.navigate([`./dashboard/users/view/${id}`]);
  }

  ngOnInit() {  
    this.currentPageNumber = Number(this.route.snapshot.params['page']);
  }

}
