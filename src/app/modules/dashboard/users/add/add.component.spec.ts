import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../../../app-routing.module';
import { AddComponent } from './add.component';
import { AuthModule } from '../../../../modules/auth/auth.module';
import { DashboardModule } from '../../../../modules/dashboard/dashboard.module';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { NgRedux, NgReduxModule } from '@angular-redux/store';

describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, NgReduxModule, ReactiveFormsModule, AppRoutingModule, AuthModule, DashboardModule, HttpModule],
      providers: [NgRedux, { provide: APP_BASE_HREF, useValue: '' }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should render title in a h5 tag', async(() => {
    const fixture = TestBed.createComponent(AddComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h5').textContent).toContain('Add New User');
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
