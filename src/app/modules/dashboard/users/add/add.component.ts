import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../../../services/users.service';
import { Router } from "@angular/router";
import { User } from '../../../../models/user.model'
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../../../store/store';
import { ADD_USER } from '../../../../store/actions/actions';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  user = { first_name: '', last_name: '', email: '', password: '' };
  userForm: FormGroup;
  actionDone = null;
  actionError = null;

  constructor(
    private router: Router, 
    private _users: UsersService,
    private ngRedux: NgRedux<IAppState>
  ) { 
    this.userForm = new FormGroup({
      'email': new FormControl(this.user.email, [Validators.required]),
      'first_name': new FormControl(this.user.first_name, Validators.required),
      'last_name': new FormControl(this.user.last_name, Validators.required),
      'password': new FormControl(this.user.password, Validators.required)
    });
  }

  ngOnInit() {
  }

  addUser(){
    this._users.add(this.user).subscribe(
      done => {
      this.ngRedux.dispatch({type: ADD_USER, user: new User(this.user)});
       this.actionError = null;
       this.actionDone = 'Thank you.';
      },
      err => {
        this.actionError = 'Error';
        this.actionDone = null;
      }
    );
  }

}
