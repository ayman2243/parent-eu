import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { PaginationModule, ModalModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../../app-routing.module';
import { FilterUsersPipe } from '../../../pipes/filter-users.pipe';

@NgModule({
  imports: [
    CommonModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule, ReactiveFormsModule,
    AppRoutingModule
  ],
  declarations: [AddComponent, ViewComponent, ListComponent, EditComponent, FilterUsersPipe]
})
export class UsersModule { }
