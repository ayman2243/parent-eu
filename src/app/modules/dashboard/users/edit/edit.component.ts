import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../../../services/users.service';
import { Router, ActivatedRoute } from "@angular/router";
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../../../store/store';
import { User } from '../../../../models/user.model';
import { UPDATE_USER } from '../../../../store/actions/actions';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @select('users') usersInStore;
  user = { first_name: '', last_name: '', email: '', password: '' };
  userForm: FormGroup;
  actionDone = null;
  actionError = null;

  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private _users: UsersService,
    private ngRedux: NgRedux<IAppState>
  ) { 
    this.getUser(this.route.snapshot.params['id']);
    this.userForm = new FormGroup({
      'email': new FormControl(this.user.email, [Validators.required]),
      'first_name': new FormControl(this.user.first_name, Validators.required),
      'last_name': new FormControl(this.user.last_name, Validators.required),
      'password': new FormControl(this.user.password, Validators.required)
    });
  }


  getUser(id){
    this._users.view(id).subscribe(
      user => {
       Object.assign(this.user, user['data'])
      },
      err => {      
        this.usersInStore.subscribe((users) =>{
          this.user = users.filter(it => it.id === this.route.snapshot.params['id'])[0];
          if(this.user === undefined){
            this.actionError = "Error, 404 not found!"
            this.actionDone = null;
            setTimeout(()=>{
              this.router.navigate([`./dashboard/users/list`]);
            }, 2000);
          }
        });
      }
    );
  }

  editUser(id = this.route.snapshot.params['id']){
    this._users.edit(id, this.user).subscribe(
      done => {
       this.ngRedux.dispatch({type: UPDATE_USER, user: new User(this.user)});
       this.actionError = null;
       this.actionDone = 'Thank you.';
      },
      err => {
        this.actionError = 'Error';
        this.actionDone = null;
      }
    );
  }

  ngOnInit() {
  }

}
