import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { UsersService } from '../../../../services/users.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../../../store/store';
import { DELETE_USER } from '../../../../store/actions/actions';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  @select('users') usersInStore;
  user: any;
  modalRef: BsModalRef;
  actionError = null;
  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private _users: UsersService,
    private modalService: BsModalService,
    private ngRedux: NgRedux<IAppState>
  ) { 
    this.getUser(this.route.snapshot.params['id']);
  }

  ngOnInit() {
  }

  deleteUser(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  confirm(id): void {
    this._users.delete(id).subscribe(
      (user) => {
        this.ngRedux.dispatch({type: DELETE_USER, id: this.route.snapshot.params['id']});
        this.modalRef.hide();
        this.router.navigate([`./dashboard/users/list/1`]);
      },
      (err) => {
        this.actionError = "Error, 404 not found!";
      }
    );
  }
 
  decline(): void {
    this.modalRef.hide();
  }

  editUser(id){
    this.router.navigate([`./dashboard/users/edit/${id}`]);
  }

  getUser(id){
    this._users.view(id).subscribe(
      (user) => {
        this.user = user['data'];
      },
      (err) =>{
        this.usersInStore.subscribe((users) =>{
          this.user = users.filter(it => it.id === this.route.snapshot.params['id'])[0];
          if(this.user === undefined){
            this.actionError = "Error, 404 not found!";
            setTimeout(()=>{
              this.router.navigate([`./dashboard/users/list`]);
            }, 2000);
          }
        });
      }
    )
  }
}
