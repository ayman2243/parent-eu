import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login = { email: '', password: '' };
  loginForm: FormGroup;

  constructor(
    private _auth: AuthService,
    private router: Router
  ) {
    this.loginForm = new FormGroup({
      'email': new FormControl(this.login.email, [Validators.required]),
      'password': new FormControl(this.login.password, Validators.required)
    });
  }

  Login(){
    this._auth.Login(this.login.email, this.login.password)
    .subscribe( data => {
      console.log(data);
      localStorage.setItem("token", data.token);
      this.router.navigate(['/dashboard']);
    }, err => {
      console.log(err);
    });
  }


  ngOnInit() {
  }

}
