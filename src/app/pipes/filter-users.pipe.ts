import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterUsers'
})
export class FilterUsersPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(args !== ''){
      let filterResult = value.filter((it) => {
        if(
          it['first_name'].toLowerCase().search(args.toLowerCase()) >= 0 ||
          it['last_name'].toLowerCase().search(args.toLowerCase()) >= 0
          )
          return it
      });
      if(filterResult.length !== 0)
        return filterResult;
      return value;
    }
    return value;
  }

}
