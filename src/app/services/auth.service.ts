import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConfig } from '../config/app.config';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: Http) { }

  Login(email: string, password: string): Observable<any> {
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers});
    return this.http.post(
      `${AppConfig.settings.apiServer.url}/login`, 
      {email, password}, 
      options).pipe(
        map((res: Response) => res.json())
      );
  }

  async isAuthenticated(){
    // check User
    if(localStorage.getItem('token'))
      return true;
    throw false;
    
  }


  Logout(){
    localStorage.clear();
  }



}
