import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user.model'; 
import { AppConfig } from '../config/app.config';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: Http) { }

  list(page: Number = 1): Observable<any> {
    let headers = new Headers;
    headers.append("access-token", `no-token`);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${AppConfig.settings.apiServer.url}/users?page=${page}`, options).pipe(
      map((res: Response) => {
        let users = {
          docs: [],
          pagination: {
            //{page: 1, per_page: 3, total: 12, total_pages: 4}
            "page": res.json()['page'],
            "per_page": res.json()['per_page'],
            "total": res.json()['total'],
            "total_pages": res.json()['total_pages']
          }
        };
        res.json()['data'].forEach(element => {
          users.docs.push(new User(element))
        });
        return users;
      })
    );
  }

  view(id: String): Observable<User> {
    let headers = new Headers;
    headers.append("access-token", `no-token`);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${AppConfig.settings.apiServer.url}/users/${id}`, options).pipe(
      map((res: Response) => new User(res.json()))
    );
  }

  add(user): Observable<any> {
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    headers.append("access-token", `no-token`);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${AppConfig.settings.apiServer.url}/users`, user, options);
  }

  edit(id: string, user): Observable<any> {
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    headers.append("access-token", `no-token`);
    let options = new RequestOptions({ headers: headers });
    return this.http.patch(`${AppConfig.settings.apiServer.url}/users/${id}`, user, options);
  }

  delete(id: string): Observable<any> {
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    headers.append("access-token", `no-token`);
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(`${AppConfig.settings.apiServer.url}/users/${id}`, options);
  }
}
