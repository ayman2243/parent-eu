import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IAppConfig } from './app-config.model';

@Injectable()
export class AppConfig {

    static settings: IAppConfig;
    private configObject = {
        dev:{
            "env": {
            "name": "production"
            },
            "apiServer": {
                "url": "https://reqres.in/api"
            }
        },
        prod:{
            "env": {
            "name": "production"
            },
            "apiServer": {
                "url": "https://reqres.in/api"
            }
        }
    }
    
    constructor() {}

    load() {
        AppConfig.settings = <IAppConfig>this.configObject[environment.name];
        return true;
    }
}